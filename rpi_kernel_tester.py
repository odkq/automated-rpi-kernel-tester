# This program build different kernel versions tailored for raspberry pi's
# and profile different subsystems on it
#
from argparse import ArgumentParser
from logging import basicConfig, info, error, INFO
from os import environ, getcwd, chdir, remove
from os.path import exists
from subprocess import run
from sys import exit
from time import time, sleep
# from fabric import Connection


class KernelInstaller():
    ''' Build and install a certain tag of the tree at
        https://github.com/raspberrypi/linux on a Raspberry Pi 4

        For this to work, the rpi must be accessible as raspberrypi.local and
        the user running the script must have access through ssh public/private
        keypair without password (see README) '''

    def __init__(self, tag, clean):
        self.tag = tag
        self.clean = clean
        self._setup_logging()
        self._install_dependencies()

    def _setup_logging(self):
        basicConfig(filename='tester.log', encoding='utf-8', level=INFO,
                    format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
        info('Starting kernel tester...')

    def _install_dependencies(self):
        ''' Install dependencies on Debian/Ubuntu '''
        if exists('.depinstalled'):
            return

        distro = self._get_distro_name()
        if distro is None:
            error("Unknown distribution (Supported Fedora/Debian)")
            exit(1)

        print(distro)
        if distro.startswith('"Debian'):
            ret = run(["sudo", "apt-get", "install", "-y", "gcc-arm-linux-gnueabihf", "bc"])
        elif distro.startswith('"Fedora'):
            ret = run(["sudo", "dnf", "install", "-y", "gcc-arm-linux-gnu.x86_64", "bc"])
        else:
            error(f"Unknown distro [{distro}] (Supported Fedora/Debian)")
            exit(1)


        if ret.returncode != 0:
            error("Error installing dependencies")
            exit(1)

        info("Dependencies installed")
        open('.depinstalled', 'w').write('installed')


    def _get_distro_name(self):
        for line in open('/etc/os-release', 'r').readlines():
            key, val = line.split('=')
            if key == 'NAME':
                return val
        return None


    def run(self, cmd, env=None, shell=False):
        ''' Convenient wrapper around subprocess.run '''
        if not shell:
            info(f' -- running {" ".join(cmd)} -- ')
        else:
            info(f' -- running {cmd} -- ')

        if env is not None:
            ret = run(cmd, env=env, shell=shell)
        else:
            ret = run(cmd, shell=shell)
        if ret.returncode != 0:
            error(f"Error executing {' '.join(cmd)}")
            exit(1)


    def build_kernel(self):
        ''' Build a certain tag for the raspberry pi kernel clone '''
        if not exists('linux'):
            self.run(["git", "clone", "https://github.com/raspberrypi/linux"])

        chdir('linux/')


        if self.tag is not None:
            self.run(["git", "checkout", self.tag])

        if self.clean:
            print('Cleaning')
            self.run(["make", "distclean"])
            if exists('arch/arm/boot/zImage'):
                remove('arch/arm/boot/zImage')
        # ARCH=arm64 CROSS_COMPILE=${CCPREFIX} make menuconfig

        # ccprefix = 'aarch64-linux-gnu-'
        ccprefix = 'arm-linux-gnueabihf-'
        environ['CCPREFIX'] = ccprefix
        environ['KERNEL_SRC'] = f'{getcwd()}/linux'
        environ['ARCH'] = 'arm'
        environ['CROSS_COMPILE'] = ccprefix

        # Recreate links to the toolchain for ccache to work
        binpath = f'{getcwd()}/bin'
        if not exists(binpath):
            self.run(['mkdir', f'{binpath}'])
            for exe in ['gcc', 'g++', 'cpp', 'c++']:
                self.run(f'ln -s /usr/bin/ccache {binpath}/arm-linux-gnueabihf-{exe}', shell=True)

        # Prepend to the PATH environment the links to ccache
        environ['PATH'] = f'{getcwd()}/bin:' + environ['PATH']

        modules_path = f'{getcwd()}/modules-{self.tag}'

        kernel = 'kernel7l'
        environ['KERNEL'] = kernel

        # env={'KERNEL': kernel, 'ARCH': 'arm', 'CROSS_COMPILE': ccprefix}
        # For Raspberry Pi 3, use bcm2709_defconfig. This is for rpi 4:
        self.run(['make', 'bcm2711_defconfig'], env=environ)
        # make -j 12 zImage modules dtbs
        if not exists('arch/arm/boot/zImage'):
            # self.run(['make', '-j', '12', 'zImage', 'modules', 'dtbs'], env=environ)
            self.run(['make', '-j', '12', 'all'], env=environ)
        # self.run(['make', '-j', '12'], env={'ARCH': 'arm', 'CROSS_COMPILE': ccprefix})
        environ['INSTALL_MOD_PATH']: f'../modules-{self.tag}'

        self.run(['rm', '-fR', f'../modules-{self.tag}'])
        self.run(['mkdir', f'../modules-{self.tag}'])

        # Install the modules in ../modules
        self.run([f'INSTALL_MOD_PATH=../modules-{self.tag} make modules_install'], shell=True, env=environ)
        # self.run(['tar', 'czpvf', '../modules.tgz', '-C', '../modules', '.'])

        self.run(['rm', '-fR', f'../boot-{self.tag}'])
        self.run(['mkdir', f'../boot-{self.tag}'])
        self.run(['mkdir', f'../boot-{self.tag}/overlays'])
        self.run(['cp', 'arch/arm/boot/zImage', f'../boot-{self.tag}/{kernel}.img'])
        self.run([f'cp arch/arm/boot/dts/*.dtb ../boot-{self.tag}/'], shell=True)
        self.run([f'cp arch/arm/boot/dts/overlays/*.dtb* ../boot-{self.tag}/overlays'], shell=True)
        self.run([f'cp arch/arm/boot/dts/overlays/README ../boot-{self.tag}/overlays'], shell=True)
        # self.run(['tar czpvf ../boot.tgz ../boot/*'], shell=True)
        chdir('..')


    def copy_kernel(self):
        ''' Copy kernel files into the raspberry pi '''
        self.run([f'find modules-{self.tag}/ -type l | xargs rm'], shell=True)
        self.run([f'scp -r modules-{self.tag}/lib root@raspberrypi.local:/'], shell=True)
        # Make a backup just in case
        self.run(["ssh root@raspberrypi.local " +
                  "'cp /boot/kernel7l.img /boot/kernel7l.img.$(date +%Y%m%d%H%M)'"], shell=True)
        self.run([f'scp -r boot-{self.tag}/* root@raspberrypi.local:/boot'], shell=True)


    def reboot(self):
        run(["ssh root@raspberrypi.local 'reboot'"], shell=True)
        sleep(3)

    def check(self):
        start = int(time())

        TIMEOUT = 60    # Currently it takes around 34 secs to reconnect
        while True:
            ret = run(["ssh root@raspberrypi.local 'uname -a'"], shell=True,
                      capture_output=True)
            now = int(time())
            if ret.returncode == 0:
                info(f'KERNEL: ({now - start} secs) {ret.stdout}')
                return ret.stdout
            if now - start >= TIMEOUT:
                error('rpi seems to be offline')
                return 'rpi seems to be offline'


def clean(mrproper=False):
    run(['rm -fR boot-*'], shell=True)
    run(['rm -fR modules-*'], shell=True)
    run(['rm -fR modules-*'], shell=True)
    run(['rm -fR linux/bin'], shell=True)
    run(['rm tester.log'], shell=True)
    if mrproper:
        run(['rm -fR linux'], shell=True)
    return 0

parser = ArgumentParser(description='Kernel compiler/tester for RPI')
parser.add_argument('--tag', '-t', dest='tag', type=str,
                    help='Tag to compile (no relation with kernel version)')
parser.add_argument('--clean', '-c', dest='clean', action='store_true',
                    help='Clean kernel local copy before compiling')
parser.add_argument('--delete', '-d', dest='delete', action='store_true',
                    help='Delete local generated files (but not the kernel tree)')
parser.add_argument('--mrproper', '-m', dest='mrproper', action='store_true',
                    help='Delete everything')
parser.add_argument('--check', '-x', dest='check', action='store_true',
                    help='Just check current version in the rpi')

args = parser.parse_args()

if args.mrproper:
    exit(clean(mrproper=True))
elif args.delete:
    exit(clean())

if not args.check and args.tag is None:
    print('Specify the tag/branch to compile.' +
          'Default branch is named rpi-5.10.y currently')
    exit(0)


start = int(time())
builder = KernelInstaller(args.tag, args.clean)

if not args.check:
    print('Before: ' + str(builder.check()))
    builder.build_kernel()
    builder.copy_kernel()
    builder.reboot()
print('KERNEL: ' + str(builder.check()))
print(f'elapsed {int(time()) - start} secs')
